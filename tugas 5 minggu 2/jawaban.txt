soal no 1

mysql> create database myshop;
Query OK, 1 row affected (0.05 sec)

soal no 2

mysql> CREATE TABLE users(id int auto_increment , name varchar(255), email varchar(255), password varchar(255), unique(id) );
Query OK, 0 rows affected (0.39 sec)


mysql> describe users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.00 sec)

mysql> CREATE TABLE categories(id int auto_increment , name varchar(255), unique(id) );
Query OK, 0 rows affected (0.37 sec)

mysql> describe categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.03 sec)

mysql> CREATE TABLE items(id int auto_increment , name varchar(255), description varchar(255), price int, stock int, category_id int, unique(id) );
Query OK, 0 rows affected (0.74 sec)

mysql> ALTER TABLE items ADD FOREIGN KEY (categories_id) REFERENCES items(id);
ERROR 1072 (42000): Key column 'categories_id' doesn't exist in table
mysql> ALTER TABLE items ADD FOREIGN KEY (category_id) REFERENCES items(id);
Query OK, 0 rows affected (0.76 sec)
Records: 0  Duplicates: 0  Warnings: 0

mysql> describe items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| price       | int(11)      | YES  |     | NULL    |                |
| stock       | int(11)      | YES  |     | NULL    |                |
| category_id | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.00 sec)

soal no 3 

mysql> INSERT INTO users (name, email, password)
    -> VALUES ("Jhon Doe", "jhon@doe.com","jhon123"),("Jane Doe", "jane@doe.com", "jenita123");
Query OK, 2 rows affected (0.05 sec)
Records: 2  Duplicates: 0  Warnings: 0

mysql> SELECT * FROM users;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  1 | Jhon Doe | jhon@doe.com | jhon123   |
|  2 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.00 sec)


mysql> INSERT INTO categories (name)
    -> VALUES ("gadget"), ("cloth"), ("men"), ("women"),("branded");
ERROR 2006 (HY000): MySQL server has gone away
No connection. Trying to reconnect...
Connection id:    2
Current database: myshop

Query OK, 5 rows affected (0.34 sec)
Records: 5  Duplicates: 0  Warnings: 0

mysql> select * from categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.00 sec)


mysql> INSERT INTO items (name, description, price, stock , category_id)
    -> VALUES ("Sumsang b50", "hape keren dari merek sumsang", "4000000", "100","1"),
    -> ("Unikloh", "baju keren dari brand ternama", "500000", "50", "2"),
    -> ("IMHO Watch", "jam tangan anak yang jujur banget", "2000000", "10", "1");
Query OK, 3 rows affected (0.35 sec)
Records: 3  Duplicates: 0  Warnings: 0

mysql> select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  2 | Unikloh     | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.00 sec)

soal no 4

a.
mysql> SELECT id, name , email FROM users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | Jhon Doe | jhon@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+

b.
1. 
mysql> SELECT * FROM items WHERE price > 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+

2.

mysql> SELECT * FROM items WHERE name like '%sang%';
+----+-------------+-------------------------------+---------+-------+-------------+
| id | name        | description                   | price   | stock | category_id |
+----+-------------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang | 4000000 |   100 |           1 |
+----+-------------+-------------------------------+---------+-------+-------------+
1 row in set (0.00 sec)

c.

mysql> SELECT items.name, items.description, items.price, items.stock, category_id,
    -> categories.name as kategori from items inner join categories on
    -> items.category_id = categories.id;
+-------------+-----------------------------------+---------+-------+-------------+----------+
| name        | description                       | price   | stock | category_id | kategori |
+-------------+-----------------------------------+---------+-------+-------------+----------+
| Sumsang b50 | hape keren dari merek sumsang     | 4000000 |   100 |           1 | gadget   |
| Unikloh     | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth    |
| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget   |
+-------------+-----------------------------------+---------+-------+-------------+----------+

soal no 5.

mysql> update items set price = 2500000 where id = 1;
Query OK, 1 row affected (0.11 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> select * from items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | description                       | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merek sumsang     | 2500000 |   100 |           1 |
|  2 | Unikloh     | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.00 sec)


